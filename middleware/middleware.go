package middleware

import (
	"fmt"
	"go-app/logging"
	"go-app/metrics"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/prometheus/client_golang/prometheus"
	"go.uber.org/zap"
)

type middleware struct {
	logger         *zap.Logger
}

func NewMiddleware(logger *zap.Logger) middleware {
	return middleware{ logger: logger}
}

// func (m middleware) NewRelicMiddleWare() gin.HandlerFunc {
// 	return nrgin.Middleware(m.newRelicConfig)
// }

// func (m middleware) SentryMiddleware() gin.HandlerFunc {
// 	return sentrygin.New(sentrygin.Options{Repanic: true})
// }

/*
Log all HTTP requests and responses to New Relic.
Generates a custom count metric for Prometheus. It uses an HTTP request path and an HTTP method.
*/
func (m middleware) LogMiddleware(ctx *gin.Context) {


	reqMethodAndPath := fmt.Sprintf("[%s] %s", ctx.Request.Method, ctx.FullPath())


	// HTTP Request Response Duration
	timer := prometheus.NewTimer(metrics.HttpRequestDuration.WithLabelValues(reqMethodAndPath))
	defer timer.ObserveDuration()

	var responseBody = logging.HandleResponseBody(ctx.Writer)
	var requestBody = logging.HandleRequestBody(ctx.Request)
	requestId := uuid.NewString()

	ctx.Next()

	statusCode := ctx.Writer.Status()
	// Same HTTP Request Path Counter
	metrics.HttpRequestCountWithPath.With(prometheus.Labels{"url": reqMethodAndPath}).Inc()
	
	logMessage := logging.FormatRequestAndResponse(statusCode, ctx.Request, responseBody.Body.String(), requestId, requestBody)

	if logMessage != "" {
		if isSuccessStatusCode(statusCode) {
			m.logger.Info(logMessage)
		} else {
			m.logger.Error(logMessage)
		}
	}
}

func isSuccessStatusCode(statusCode int) bool {
	switch statusCode {
	case http.StatusOK, http.StatusCreated, http.StatusAccepted, http.StatusNoContent:
		return true
	default:
		return false
	}
}
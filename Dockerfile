# syntax=docker/dockerfile:1
FROM --platform=$BUILDPLATFORM golang:alpine AS build
WORKDIR /src

RUN apk add -U tzdata
RUN apk --update add ca-certificates

COPY . .
# COPY .env .env

# COPY go.mod go.sum .
RUN go mod download
RUN go mod verify
RUN go version

# ARG APP_ENV
# ENV APP_ENV=${APP_ENV}

# ARG APP_CONF
# ENV APP_CONF=${APP_CONF}


ARG TARGETOS TARGETARCH
RUN --mount=target=. \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go build -o /server ./cmd/server

# RUN cat config/local.yml > /local.yml
# RUN cat config/prod.yml > /prod.yml 
# RUN cat config/test.yml > /test.yml 
# RUN pwd
# RUN ls -trla
# RUN cat .secure_files/test.yml  > /test.yml
# COPY  .secure_files /.secure_files
# RUN echo "SECURE_FILES_DOWNLOAD_PATH"
# RUN echo $SECURE_FILES_DOWNLOAD_PATH
# COPY 

FROM scratch

# ARG APP_CONF
# ENV APP_CONF=${APP_CONF}

# COPY --from=build  /usr/share/zoneinfo/Europe/Helsinki /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
# COPY --from=build ["/prod.yml","/config/prod.yml"]
# COPY --from=build ["/test.yml","/config/test.yml"]
# COPY --from=build /src/.env .
COPY --from=build /usr/local/go/lib/time/zoneinfo.zip /
ENV TZ=Europe/Helsinki
ENV ZONEINFO=/zoneinfo.zip

COPY --from=build /server .

EXPOSE 8080
CMD ["/server"]

package config

import (
	"context"
	"github.com/sethvargo/go-envconfig"
	"log"
	"sync"
)

var (
	cfg        AppConfig
	configOnce sync.Once
)

func config() AppConfig {
	configOnce.Do(func() {
		ctx := context.Background()
		if err := envconfig.Process(ctx, &cfg); err != nil {
			log.Fatal(err)
		}
		log.Println("Environments initialized.")
	})
	return cfg
}

type AppConfig struct {
	Database *Database
	// NewRelic *NewRelic
	// Sentry   *Sentry
}

type Database struct {
	Host         string `env:"POSTGRES_HOST, default=cluster-pg-rw.postgres-server-prod.svc.cluster.local"`
	Username     string `env:"POSTGRES_USERNAME, default=app"`
	Password     string `env:"POSTGRES_PASSWORD, default=05GNPpsZkgvWCRveZe1QKz8QRP4WxMgabpD2ETiRUsv8snHe5rK3LnsJkIv3IWoJ"`
	Port         string `env:"POSTGRES_PORT, default=5432"`
	DatabaseName string `env:"DATABASE_NAME, default=app"`
}

// type NewRelic struct {
// 	AppName string `env:"APP_NAME, default=go-app"`
// 	License string `env:"NEW_RELIC_LICENSE, default=eu01xx489166739db843beeeb65452b2CCCCNRAL"`
// }

// type Sentry struct {
// 	Dsn string `env:"SENTRY_DSN"`
// }